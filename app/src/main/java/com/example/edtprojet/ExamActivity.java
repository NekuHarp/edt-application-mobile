package com.example.edtprojet;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;

public class ExamActivity extends AppCompatActivity {
    public ArrayAdapter<String> adapter = null;
    public String[] values;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam);
        AgendaParser.dateToDay();
        ListView listview = findViewById(R.id.listviewExam);
        FloatingActionButton refresh = findViewById(R.id.refreshbtn);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AgendaParser.createTxtFromUrl(getApplicationContext());
            }
        });
        ArrayList<Cours> listNextExam = AgendaParser.getListExam(AgendaParser.laDate, "L3INFO_TD1", getApplicationContext());
        if (!listNextExam.isEmpty()) {
            this.values = AgendaParser.getStringCoursDecriptionWithDate(listNextExam);
            this.adapter = AgendaParser.getArrayAdapter(this, this.values);
            listview.setAdapter(this.adapter);
        }
    }
}