package com.example.edtprojet;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class ConfigActivity extends AppCompatActivity {

    public Button btnSave;
    public String leGroupe = BuildConfig.FLAVOR;
    public String leUE = BuildConfig.FLAVOR;
    public RadioGroup gradiotd;
    public RadioGroup gradioue;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        SharedPreference.createDefault(this);
        initAllComponent();
        checkRadioPreference();
        initAllListener();

    }

    public void checkRadioPreference() {

        SharedPreferences sharedPreferences = getSharedPreferences(SharedPreference.PREFS, 0);
        String groupe = sharedPreferences.getString(SharedPreference.PREFS_GROUPE, BuildConfig.FLAVOR);
        String ue = sharedPreferences.getString(SharedPreference.PREFS_UE, BuildConfig.FLAVOR);

    }

    public void initAllComponent() {

        this.btnSave = (Button) findViewById(R.id.btnSave);
        this.gradiotd = (RadioGroup) findViewById(R.id.radiogroup1);
        this.gradioue = (RadioGroup) findViewById(R.id.radiogroup2);

    }

    public void initAllListener() {

        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                RadioButton checkedgroupe = findViewById(gradiotd.getCheckedRadioButtonId());
                leGroupe = checkedgroupe.getText().toString();
                RadioButton checkedue = findViewById(gradioue.getCheckedRadioButtonId());
                leUE = checkedue.getText().toString();
                SharedPreference.modifAllValues(getApplicationContext(), leGroupe, leUE);
                Toast.makeText(getApplicationContext(), "Enregistré", Toast.LENGTH_LONG).show();

            }

        });

    }

}