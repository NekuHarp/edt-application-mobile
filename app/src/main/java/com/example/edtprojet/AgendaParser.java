package com.example.edtprojet;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

public final class AgendaParser {

    public static boolean isDataSet = false;
    static Date c = Calendar.getInstance().getTime();
    static SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
    public static String laDate = df.format(c);
    public static Context leContext;
    public static String ue;
    public static String urlAgenda = "https://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN";

    private AgendaParser() {

    }

    public static String getHeure(String line) {

        StringBuilder BheureDebut = new StringBuilder();
        BheureDebut.append(line.substring(line.lastIndexOf(84) + 1, line.length() - 5));
        BheureDebut.append(":");
        BheureDebut.append(line.substring(line.lastIndexOf(84) + 3, line.length() - 3));
        String heureDebut = BheureDebut.toString();

        StringBuilder BdateRecup = new StringBuilder();
        BdateRecup.append(line.substring(line.indexOf(":") + 1, line.indexOf(":") + 5));
        BdateRecup.append("-");
        BdateRecup.append(line.substring(line.indexOf(":") + 5, line.indexOf(":") + 7));
        BdateRecup.append("-");
        BdateRecup.append(line.substring(line.indexOf(":") + 7, line.indexOf(":") + 9));
        String dateRecup = BdateRecup.toString();

        StringBuilder BgoodDate = new StringBuilder();
        BgoodDate.append(dateRecup);
        BgoodDate.append(" ");
        BgoodDate.append(heureDebut);
        String goodDate = BgoodDate.toString();

        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parsed = null;
        try {
            parsed = sourceFormat.parse(goodDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        TimeZone tz = TimeZone.getTimeZone("Europe/Paris");
        SimpleDateFormat destFormat = new SimpleDateFormat("HH:mm");
        destFormat.setTimeZone(tz);
        return destFormat.format(parsed).replace(":", BuildConfig.FLAVOR);

    }

    public static ArrayList<Cours> getListCours(String day, String groupe, Context context) {

        FileNotFoundException e;
        IOException e2;
        String str = day;
        Context context2 = context;
        int i = 0;
        ue = context2.getSharedPreferences(SharedPreference.PREFS, 0).getString(SharedPreference.PREFS_UE, BuildConfig.FLAVOR);
        createFileIfNotExist(context);
        ArrayList<Cours> coursDuJour = new ArrayList();
        String str2;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(context2.openFileInput("etd.txt")));
            while (true) {
                String readLine = br.readLine();
                String line = readLine;
                if (readLine == null) {
                    str2 = groupe;
                    break;
                }
                boolean stopCurrentLoop = false;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("DTSTART;VALUE=DATE:");
                stringBuilder.append(str);
                if (line.startsWith(stringBuilder.toString())) {
                    coursDuJour.add(new Cours("ferie", "ferie", "ferie", "ferie", "ferie", "ferie", "ferie", "ferie"));
                    str2 = groupe;
                    break;
                }
                stringBuilder = new StringBuilder();
                stringBuilder.append("DTSTART:");
                stringBuilder.append(str);
                if (line.startsWith(stringBuilder.toString())) {
                    String jour = line.substring(line.indexOf(58) + 1, line.lastIndexOf(84));
                    String heureDebut = getHeure(line);
                    String heureFin = getHeure(br.readLine());
                    line = br.readLine();
                    if (line.contains("UEO")) {
                        coursDuJour.add(new Cours(jour, heureDebut, heureFin, line.substring(line.indexOf(":") + 1, line.lastIndexOf("-") - 1), "UNDEFINED", line.substring(line.lastIndexOf("-") + 2, line.length()).replace("\\", BuildConfig.FLAVOR), "UNDEFINED", "UEO"));
                        stopCurrentLoop = true;
                    }
                    try {
                        if ((line.contains(groupe) || line.contains("L3 INFORMATIQUE") || line.contains(ue)) && !stopCurrentLoop) {
                            String type = line.substring(line.lastIndexOf(45) + 2, line.length());
                            line = line.substring(i, line.lastIndexOf(45) - 1);
                            String promotion = line.substring(line.lastIndexOf(45) + 2, line.length()).replace("\\", BuildConfig.FLAVOR);
                            String line2 = line.substring(0, line.lastIndexOf(45) - 1);
                            if (line2.contains("-")) {
                                line = line2.substring(line2.lastIndexOf(45) + 2, line2.length()).replace("\\", BuildConfig.FLAVOR);
                            } else {
                                line = "UNDEFINED";
                            }
                            if (line2.contains("-")) {
                                line2 = line2.substring(0, line2.lastIndexOf(45) - 1);
                            }
                            String matiere = line2.substring(line2.indexOf(58) + 1, line2.length()).replace("\\", BuildConfig.FLAVOR);
                            line2 = br.readLine();
                            coursDuJour.add(new Cours(jour, heureDebut, heureFin, matiere, line, promotion, line2.substring(line2.lastIndexOf(58) + 1, line2.length()).replace("\\", BuildConfig.FLAVOR), type));
                        }
                    } catch (FileNotFoundException e3) {
                        e = e3;
                        e.printStackTrace();
                    } catch (IOException e4) {
                        e2 = e4;
                        e2.printStackTrace();
                    }
                }
                str2 = groupe;
                i = 0;
            }
            br.close();
        } catch (FileNotFoundException e5) {
            e = e5;
            str2 = groupe;
            e.printStackTrace();
        } catch (IOException e6) {
            e2 = e6;
            str2 = groupe;
            e2.printStackTrace();
        }
        Collections.sort(coursDuJour, new Comparator<Cours>() {
            public int compare(Cours o1, Cours o2) {
                return o1.getHeureDebut().compareTo(o2.getHeureDebut());
            }
        });
        return coursDuJour;
    }

    public static ArrayList<Cours> getListExam(String day, String groupe, Context context) {

        FileNotFoundException e;
        IOException e2;
        Context context2 = context;
        int i = 0;
        ue = context2.getSharedPreferences(SharedPreference.PREFS, 0).getString(SharedPreference.PREFS_UE, BuildConfig.FLAVOR);
        createFileIfNotExist(context);
        ArrayList<Cours> coursDuJour = new ArrayList();
        String str;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(context2.openFileInput("etd.txt")));
            int currentDate = Integer.parseInt(day);
            while (true) {
                String readLine = br.readLine();
                String line = readLine;
                if (readLine == null) {
                    break;
                }
                if (!line.contains("DTSTART:20")) {
                    str = groupe;
                } else if (Integer.parseInt(line.substring(line.indexOf(":") + 1, line.indexOf(":") + 9)) > currentDate) {
                    String jour = line.substring(line.indexOf(58) + 1, line.lastIndexOf(84));
                    String heureDebut = getHeure(line);
                    String heureFin = getHeure(br.readLine());
                    line = br.readLine();
                    try {
                        if ((line.contains(groupe) || line.contains("L3 INFORMATIQUE") || line.contains(ue)) && line.contains("Evaluation") && !line.contains("Annulation")) {
                            String type = line.substring(line.lastIndexOf(45) + 2, line.length());
                            line = line.substring(i, line.lastIndexOf(45) - 1);
                            String promotion = line.substring(line.lastIndexOf(45) + 2, line.length()).replace("\\", BuildConfig.FLAVOR);
                            String line2 = line.substring(0, line.lastIndexOf(45) - 1);
                            if (line2.contains("-")) {
                                line = line2.substring(line2.lastIndexOf(45) + 2, line2.length()).replace("\\", BuildConfig.FLAVOR);
                            } else {
                                line = "UNDEFINED";
                            }
                            if (line2.contains("-")) {
                                line2 = line2.substring(0, line2.lastIndexOf(45) - 1);
                            }
                            String matiere = line2.substring(line2.indexOf(58) + 1, line2.length());
                            line2 = br.readLine();
                            coursDuJour.add(new Cours(jour, heureDebut, heureFin, matiere, line, promotion, line2.substring(line2.lastIndexOf(58) + 1, line2.length()).replace("\\", BuildConfig.FLAVOR), type));
                        }
                    } catch (FileNotFoundException e3) {
                        e = e3;
                        e.printStackTrace();
                    } catch (IOException e4) {
                        e2 = e4;
                        e2.printStackTrace();
                    }
                } else {
                    str = groupe;
                }
                i = 0;
            }
            str = groupe;
            br.close();
        } catch (FileNotFoundException e5) {
            e = e5;
            str = groupe;
            e.printStackTrace();
        } catch (IOException e6) {
            e2 = e6;
            str = groupe;
            e2.printStackTrace();
        }
        Collections.sort(coursDuJour, new Comparator<Cours>() {
            public int compare(Cours o1, Cours o2) {
                return o1.getJour().compareTo(o2.getJour());
            }
        });
        return coursDuJour;
    }

    public static void createFileIfNotExist(Context context) {
        String[] listFichier = context.fileList();
        int i = 0;
        while (i < listFichier.length) {
            if (!listFichier[i].equals("etd.txt")) {
                i++;
            } else {
                return;
            }
        }
        createTxtFromUrl(context);
    }

    public static void createTxtFromUrl(Context context) {
        final Context context1 = context;
        new AsyncTask<String, Void, Void>() {
            @Override
            public void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            public Void doInBackground(String... params) {
                try {
                    URL url = new URL(urlAgenda);
                    HttpURLConnection c = (HttpURLConnection) url.openConnection();
                    HttpURLConnection.setFollowRedirects(false);
                    c.setInstanceFollowRedirects(false);
                    c.setDoOutput(false);
                    c.setUseCaches(true);
                    c.setRequestMethod("GET");
                    c.connect();
                    FileOutputStream f = context1.openFileOutput("etd.txt", 0);
                    InputStream in = c.getInputStream();
                    byte[] buffer = new byte[1024];
                    int len1 = 0;
                    while (true) {
                        int read = in.read(buffer);
                        len1 = read;
                        if (read <= 0) {
                            break;
                        }
                        f.write(buffer, 0, len1);
                    }
                    f.close();
                    in.close();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    Log.d("aaa", "bbbbbbbb");
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("aaa", "cccccccccc");
                }
                return null;
            }

            @Override
            public void onPostExecute(Void Void) {

            }
        }.execute("");
    }


    public static String[] getStringCoursDescription(ArrayList<Cours> listCours) {
        String[] values = new String[listCours.size()];
        for (int i = 0; i < listCours.size(); i++) {
            values[i] = ((Cours) listCours.get(i)).description();
        }
        return values;
    }

    public static String[] getStringCoursDecriptionWithDate(ArrayList<Cours> listExam) {
        String[] values = new String[listExam.size()];
        for (int i = 0; i < listExam.size(); i++) {
            values[i] = ((Cours) listExam.get(i)).descriptionAvecDate();
        }
        return values;
    }

    public static void choiceDate(Context context) {
        leContext = context;
        Calendar c = Calendar.getInstance();
        int day = c.get(5);
        int month = c.get(2);
        int year = c.get(1);
        DatePickerDialog dpg = new DatePickerDialog(context, new OnDateSetListener() {
            public void onDateSet(DatePicker view, int myear, int mmonth, int mdayOfMonth) {
                StringBuilder stringBuilder;
                String jour = String.valueOf(mdayOfMonth);
                if (jour.length() == 1) {
                    StringBuilder stringBuilder2 = new StringBuilder();
                    stringBuilder2.append("0");
                    stringBuilder2.append(jour);
                    jour = stringBuilder2.toString();
                }
                String mois = String.valueOf(mmonth + 1);
                if (mois.length() == 1) {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("0");
                    stringBuilder.append(mois);
                    mois = stringBuilder.toString();
                }
                stringBuilder = new StringBuilder();
                stringBuilder.append(String.valueOf(myear));
                stringBuilder.append(mois);
                stringBuilder.append(jour);
                AgendaParser.laDate = stringBuilder.toString();
                AgendaParser.isDataSet = true;
            }
        }, day, month, year);
        dpg.updateDate(year, month, day);
        dpg.show();
        dpg.setOnDismissListener(new OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
                if (AgendaParser.isDataSet) {
                    AgendaParser.leContext.startActivity(new Intent(AgendaParser.leContext, DateCoursActivity.class));
                }
            }
        });
    }

    public static String transformeDateToLetterDate() {
        String dateLettre = BuildConfig.FLAVOR;
        String jourLettre = BuildConfig.FLAVOR;
        String moisLettre = BuildConfig.FLAVOR;
        String jour = laDate;
        jour = jour.substring(6, jour.length());
        String mois = laDate.substring(4, 6);
        String annee = laDate.substring(0, 4);
        int leMois = Integer.parseInt(mois);
        String str = laDate;
        String transDate = str.substring(6, str.length()) +
                "/" +
                laDate.substring(4, 6) +
                "/" +
                laDate.substring(0, 4);
        Calendar c = Calendar.getInstance();
        Date uneDate = null;
        try {
            uneDate = new SimpleDateFormat("dd/M/yyyy").parse(transDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(uneDate);
        switch (c.get(7)) {
            case 1:
                jourLettre = "Dimanche";
                break;
            case 2:
                jourLettre = "Lundi";
                break;
            case 3:
                jourLettre = "Mardi";
                break;
            case 4:
                jourLettre = "Mercredi";
                break;
            case 5:
                jourLettre = "Jeudi";
                break;
            case 6:
                jourLettre = "Vendredi";
                break;
            case 7:
                jourLettre = "Samedi";
                break;
            default:
                jourLettre = "Undefined";
                break;
        }
        switch (leMois) {
            case 1:
                moisLettre = "Janvier";
                break;
            case 2:
                moisLettre = "Février";
                break;
            case 3:
                moisLettre = "Mars";
                break;
            case 4:
                moisLettre = "Avril";
                break;
            case 5:
                moisLettre = "Mai";
                break;
            case 6:
                moisLettre = "Juin";
                break;
            case 7:
                moisLettre = "Juillet";
                break;
            case 8:
                moisLettre = "Août";
                break;
            case 9:
                moisLettre = "Septembre";
                break;
            case 10:
                moisLettre = "Octobre";
                break;
            case 11:
                moisLettre = "Novembre";
                break;
            case 12:
                moisLettre = "Décembre";
                break;
            default:
                moisLettre = "Undefined";
                break;
        }
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(jourLettre);
        stringBuilder2.append(" ");
        stringBuilder2.append(jour);
        stringBuilder2.append(" ");
        stringBuilder2.append(moisLettre);
        stringBuilder2.append(" ");
        stringBuilder2.append(annee);
        return stringBuilder2.toString();
    }

    public static ArrayAdapter<String> getArrayAdapter(Context context, String[] values) {
        return new ArrayAdapter<String>(context, R.layout.adapter_cours, new ArrayList(Arrays.asList(values))) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView tv = view.findViewById(R.id.textviewdesccours);
                tv.setTextColor(-1);
                boolean nothing = true;
                if (tv.getText().toString().contains("Evaluation")) {
                    view.setBackgroundResource(R.drawable.background_evaluation);
                    nothing = false;
                }
                if (tv.getText().toString().contains("Type: TD") || tv.getText().toString().contains("Type: TP")) {
                    view.setBackgroundResource(R.drawable.background_td);
                    nothing = false;
                }
                if (tv.getText().toString().contains("Type: CM")) {
                    view.setBackgroundResource(R.drawable.background_cm);
                    nothing = false;
                }
                if (tv.getText().toString().toLowerCase().contains("annulation")) {
                    view.setBackgroundResource(R.drawable.background_canceled);
                    nothing = false;
                }
                if (tv.getText().toString().contains("UEO")) {
                    view.setBackgroundResource(R.drawable.background_ueo);
                    nothing = false;
                }
                if (nothing) {
                    view.setBackgroundResource(R.drawable.background_default);
                }
                return view;
            }
        };
    }

    public static void changeDate(int add) {
        String str = laDate;
        StringBuilder stringBuilder;
        stringBuilder = new StringBuilder();
        stringBuilder.append(str.substring(6, str.length()));
        stringBuilder.append("/");
        stringBuilder.append(laDate.substring(4, 6));
        stringBuilder.append("/");
        stringBuilder.append(laDate.substring(0, 4));
        String transDate = stringBuilder.toString();
        Calendar c = Calendar.getInstance();
        Date uneDate = null;
        try {
            uneDate = new SimpleDateFormat("dd/M/yyyy").parse(transDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(uneDate);
        c.add(5, add);
        uneDate = c.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy");
        DateFormat dateFormat2 = new SimpleDateFormat("M");
        DateFormat dateFormat3 = new SimpleDateFormat("dd");
        String year = dateFormat.format(uneDate);
        String mois = dateFormat2.format(uneDate);
        String jour = dateFormat3.format(uneDate);
        if (jour.length() == 1) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("0");
            stringBuilder.append(jour);
            jour = stringBuilder.toString();
        }
        if (mois.length() == 1) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("0");
            stringBuilder.append(mois);
            mois = stringBuilder.toString();
        }
        stringBuilder = new StringBuilder();
        stringBuilder.append(year);
        stringBuilder.append(mois);
        stringBuilder.append(jour);
        laDate = stringBuilder.toString();
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Date : ");
        stringBuilder2.append(laDate);
        Log.d("TEST", stringBuilder2.toString());
    }

    public static String transformeDateToNormalDate(String uneDate) {
        StringBuilder BtransDate = new StringBuilder();
        BtransDate.append(uneDate.substring(6, uneDate.length()));
        BtransDate.append("/");
        BtransDate.append(uneDate.substring(4, 6));
        BtransDate.append("/");
        BtransDate.append(uneDate.substring(0, 4));
        String transDate = BtransDate.toString();
        return transDate;
    }

    public static void dateToDay() {
        StringBuilder stringBuilder;
        Calendar calander = Calendar.getInstance();
        int cDay = calander.get(5);
        int cMonth = calander.get(2) + 1;
        int cYear = calander.get(1);
        String sDay = String.valueOf(cDay);
        if (sDay.length() == 1) {
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("0");
            stringBuilder2.append(sDay);
            sDay = stringBuilder2.toString();
        }
        String sMonth = String.valueOf(cMonth);
        if (sMonth.length() == 1) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("0");
            stringBuilder.append(sMonth);
            sMonth = stringBuilder.toString();
        }
        stringBuilder = new StringBuilder();
        stringBuilder.append(String.valueOf(cYear));
        stringBuilder.append(sMonth);
        stringBuilder.append(sDay);
        laDate = stringBuilder.toString();
    }
}
