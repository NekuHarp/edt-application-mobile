package com.example.edtprojet;


public class Cours {

    private String enseignant;
    private String heureDebut;
    private String heureFin;
    private String jour;
    private String matiere;
    private String promotion;
    private String salle;
    private String type;

    public Cours(String d, String hd, String hf, String m, String e, String p, String s, String t) {

        this.jour = d;
        this.heureDebut = hd;
        this.heureFin = hf;
        this.matiere = m;
        this.enseignant = e;
        this.promotion = p;
        this.salle = s;
        this.type = t;

    }

    public String description() {

        StringBuilder stringBuilder;
        String hd = this.heureDebut;
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(this.heureDebut.substring(0, 2));
        stringBuilder2.append(":");
        String str = this.heureDebut;
        stringBuilder2.append(str.substring(2, str.length()));
        hd = stringBuilder2.toString();
        String hf = this.heureFin;
        StringBuilder stringBuilder3 = new StringBuilder();
        stringBuilder3.append(this.heureFin.substring(0, 2));
        stringBuilder3.append(":");
        String str2 = this.heureFin;
        stringBuilder3.append(str2.substring(2, str2.length()));
        hf = stringBuilder3.toString();
        str = BuildConfig.FLAVOR;

        if (!this.matiere.equals("UNDEFINED")) {

            stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append("Matière: ");
            stringBuilder.append(this.matiere);
            stringBuilder.append("\n");
            str = stringBuilder.toString();

        }

        stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append("Horaire: ");
        stringBuilder.append(hd);
        stringBuilder.append(" à ");
        stringBuilder.append(hf);
        stringBuilder.append("\n");
        str = stringBuilder.toString();

        if (!this.enseignant.equals("UNDEFINED")) {

            stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append("Enseignant: ");
            stringBuilder.append(this.enseignant);
            stringBuilder.append("\n");
            str = stringBuilder.toString();

        }

        if (!this.promotion.equals("UNDEFINED")) {

            stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append("TD: ");
            stringBuilder.append(this.promotion);
            stringBuilder.append("\n");
            str = stringBuilder.toString();

        }

        if (!this.salle.equals("UNDEFINED")) {

            stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append("Salle: ");
            stringBuilder.append(this.salle);
            stringBuilder.append("\n");
            str = stringBuilder.toString();

        }

        if (this.type.equals("UNDEFINED")) {

            return str;

        }

        stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append("Type: ");
        stringBuilder.append(this.type);
        return stringBuilder.toString();

    }

    public String descriptionAvecDate() {

        StringBuilder stringBuilder;
        String hd = this.heureDebut;
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(this.heureDebut.substring(0, 2));
        stringBuilder2.append(":");
        String str = this.heureDebut;
        stringBuilder2.append(str.substring(2, str.length()));
        hd = stringBuilder2.toString();
        String hf = this.heureFin;
        StringBuilder stringBuilder3 = new StringBuilder();
        stringBuilder3.append(this.heureFin.substring(0, 2));
        stringBuilder3.append(":");
        String str2 = this.heureFin;
        stringBuilder3.append(str2.substring(2, str2.length()));
        hf = stringBuilder3.toString();
        str = BuildConfig.FLAVOR;
        str2 = AgendaParser.transformeDateToNormalDate(this.jour);

        if (!this.matiere.equals("UNDEFINED")) {

            stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append("Matière: ");
            stringBuilder.append(this.matiere);
            stringBuilder.append("\n");
            str = stringBuilder.toString();

        }

        stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append("Date: ");
        stringBuilder.append(str2);
        stringBuilder.append("\n");
        str = stringBuilder.toString();
        stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append("Horaire: ");
        stringBuilder.append(hd);
        stringBuilder.append(" à ");
        stringBuilder.append(hf);
        stringBuilder.append("\n");
        str = stringBuilder.toString();

        if (!this.enseignant.equals("UNDEFINED")) {

            stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append("Enseignant: ");
            stringBuilder.append(this.enseignant);
            stringBuilder.append("\n");
            str = stringBuilder.toString();

        }

        if (!this.promotion.equals("UNDEFINED")) {

            stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append("TD: ");
            stringBuilder.append(this.promotion);
            stringBuilder.append("\n");
            str = stringBuilder.toString();

        }

        if (!this.salle.equals("UNDEFINED")) {

            stringBuilder = new StringBuilder();
            stringBuilder.append(str);
            stringBuilder.append("Salle: ");
            stringBuilder.append(this.salle);
            stringBuilder.append("\n");
            str = stringBuilder.toString();

        }

        if (this.type.equals("UNDEFINED")) {

            return str;

        }

        stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append("Type: ");
        stringBuilder.append(this.type);
        return stringBuilder.toString();

    }

    public int getJourInt() {

        return Integer.parseInt(this.jour);

    }

    public int getHeureDebutInt() {

        return Integer.parseInt(this.heureDebut);

    }

    public String getMatiere() {

        return this.matiere;

    }

    public String getJour() {

        return this.jour;

    }

    public void setJour(String jour) {

        this.jour = jour;

    }

    public String getHeureDebut() {

        return this.heureDebut;

    }

    public void setHeureDebut(String heureDebut) {

        this.heureDebut = heureDebut;

    }

    public String getHeureFin() {

        return this.heureFin;

    }

    public void setHeureFin(String heureFin) {

        this.heureFin = heureFin;

    }

    public void setMatiere(String matiere) {

        this.matiere = matiere;

    }

    public String getEnseignant() {

        return this.enseignant;

    }

    public void setEnseignant(String enseignant) {

        this.enseignant = enseignant;

    }

    public String getPromotion() {

        return this.promotion;

    }

    public void setPromotion(String promotion) {

        this.promotion = promotion;

    }

    public String getSalle() {

        return this.salle;

    }

    public void setSalle(String salle) {

        this.salle = salle;

    }

    public String getType() {

        return this.type;

    }

    public void setType(String type) {

        this.type = type;

    }

}
