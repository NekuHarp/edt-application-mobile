package com.example.edtprojet;

import android.content.Context;
import android.content.SharedPreferences;

public final class SharedPreference {

    public static final String PREFS = "PREFS";
    public static final String PREFS_GROUPE = "PREFS_GROUPE";
    public static final String PREFS_UE = "PREFS_UE";

    public static void createDefault(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS, 0);
        if (!sharedPreferences.contains(PREFS_GROUPE)) {
            sharedPreferences.edit().putString(PREFS_GROUPE, "L3INFO_TD1").putString(PREFS_UE, "UE PROJET PROGRAMMATION 2").apply();
        }
    }

    public static void modifAllValues(Context context, String groupe, String ue) {
        context.getSharedPreferences(PREFS, 0).edit().putString(PREFS_GROUPE, groupe).putString(PREFS_UE, ue).apply();
    }
}