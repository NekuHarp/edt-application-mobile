package com.example.edtprojet;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public ArrayAdapter<String> adapter = null;
    public String groupe;
    public SharedPreferences sharedPreferences;
    TextView textDate;
    public String[] values;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.sharedPreferences = getSharedPreferences(SharedPreference.PREFS, 0);
        this.groupe = this.sharedPreferences.getString(SharedPreference.PREFS_GROUPE, BuildConfig.FLAVOR);

        Button agendabtn = findViewById(R.id.agendabtn);
        agendabtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, DateCoursActivity.class);
                startActivity(i);
            }
        });

        Button exambtn = findViewById(R.id.exambtn);
        exambtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ExamActivity.class);
                startActivity(i);
            }
        });

        Button configbtn = findViewById(R.id.configbtn);
        configbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ConfigActivity.class);
                startActivity(i);
            }
        });

    }
}
