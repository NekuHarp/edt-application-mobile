package com.example.edtprojet;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;

public class DateCoursActivity extends AppCompatActivity {

    public ArrayAdapter<String> adapter = null;
    public String groupe;
    ImageView imgPasCours;
    ListView listview;
    public SharedPreferences sharedPreferences;
    TextView textDate;
    FloatingActionButton refresh;
    public String[] values;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_cours);
        this.sharedPreferences = getSharedPreferences(SharedPreference.PREFS, 0);
        this.groupe = this.sharedPreferences.getString(SharedPreference.PREFS_GROUPE, BuildConfig.FLAVOR);
        String dateLettre = AgendaParser.transformeDateToLetterDate();
        this.listview = findViewById(R.id.listviewDate);
        this.textDate = findViewById(R.id.textDateCours);
        this.textDate.setText(dateLettre);
        this.textDate.setGravity(1);
        this.imgPasCours = findViewById(R.id.imgPasCours);
        this.refresh = findViewById(R.id.refreshbtn);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AgendaParser.createTxtFromUrl(getApplicationContext());
            }
        });

        initAdapter();
        (findViewById(R.id.leftchevron)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AgendaParser.changeDate(-1);
                DateCoursActivity.this.updateActivity();
            }
        });
        (findViewById(R.id.rightchevron)).setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                AgendaParser.changeDate(1);
                DateCoursActivity.this.updateActivity();
            }
        });
    }

    public void initAdapter() {
        ArrayList<Cours> coursDuJour = AgendaParser.getListCours(AgendaParser.laDate, this.groupe, getApplicationContext());
        if (coursDuJour.isEmpty()) {
            if (this.textDate.getText().toString().contains("Dimanche") || this.textDate.getText().toString().contains("Samedi")) {
                this.imgPasCours.setImageResource(R.drawable.background_weekend);
            } else {
                this.imgPasCours.setImageResource(R.drawable.background_noclass);
            }
        } else if (((Cours) coursDuJour.get(0)).description().contains("ferie")) {
            this.imgPasCours.setImageResource(R.drawable.background_holidays);
        } else {
            this.imgPasCours.setVisibility(View.INVISIBLE);
            this.values = AgendaParser.getStringCoursDescription(coursDuJour);
            this.adapter = AgendaParser.getArrayAdapter(this, this.values);
            this.listview.setAdapter(this.adapter);
        }
    }

    public void updateActivity() {
        if (this.adapter != null) {
            this.imgPasCours.setVisibility(View.INVISIBLE);
            this.values = AgendaParser.getStringCoursDescription(AgendaParser.getListCours(AgendaParser.laDate, this.groupe, getApplicationContext()));
            ArrayList<String> value = new ArrayList(Arrays.asList(this.values));
            this.adapter.clear();
            this.textDate.setText(AgendaParser.transformeDateToLetterDate());
            if (value.isEmpty()) {
                this.imgPasCours.setVisibility(View.VISIBLE);
                if (this.textDate.getText().toString().contains("Dimanche") || this.textDate.getText().toString().contains("Samedi")) {
                    this.imgPasCours.setImageResource(R.drawable.background_weekend);
                    return;
                }
                this.imgPasCours.setVisibility(View.VISIBLE);
                this.imgPasCours.setImageResource(R.drawable.background_noclass);
                return;
            } else if ((value.get(0)).contains("ferie")) {
                this.imgPasCours.setVisibility(View.VISIBLE);
                this.imgPasCours.setImageResource(R.drawable.background_holidays);
                return;
            } else {
                this.adapter.addAll(value);
                this.adapter.notifyDataSetChanged();
                return;
            }
        }
        this.textDate.setText(AgendaParser.transformeDateToLetterDate());
        initAdapter();
    }
}
